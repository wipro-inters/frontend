#!/bin/python
import requests
from functools import reduce
import re

BASE_URL = 'http://localhost:8080'
SWAGGER_URL = 'http://165.227.107.109/v2/api-docs'

class Endpoint:

    def __init__(self, path, description):
        self.path = path
        self.description = description

    @classmethod
    def from_swagger_tag(cls, path, tag):
        return cls(path, tag.replace('-rest', ''))


def get_swagger_json(url):
    r = requests.get(url)
    return r.json()

def parse_endpoints(json):
    """Receive json dict, return list of Endpoints"""
    paths = json['paths']
    endpoint_path_factory = lambda path, methods : [Endpoint.from_swagger_tag(path, endpoint_data['tags'][0])
                                                    for _, endpoint_data in methods.items()]
    endpoints_nested = [endpoint_path_factory(path, methods) for path, methods in paths.items()]
    endpoints = reduce(lambda xs, ys : xs + ys, endpoints_nested, [])
    return endpoints


def format_endpoints(prefix, endpoints):
    fmt = 'export const {} = "{}{}";'
    return [fmt.format(endpoint.description, prefix, endpoint.path) for endpoint in endpoints]

def format_file(endpoints):
    txt = '\n'.join(endpoints)
    txt = re.sub(r'\{.*\}', '{}', txt)
    txt = txt.replace('-', '_')
    return txt

def main():
    json = get_swagger_json(SWAGGER_URL)
    endpoints = parse_endpoints(json)
    endpoints_formatted = format_endpoints(BASE_URL, endpoints)
    print(format_file(endpoints_formatted))
    
if __name__ == '__main__':
    main()
