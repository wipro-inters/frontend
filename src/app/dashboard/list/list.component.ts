import { Component, OnInit } from '@angular/core';
import { InterviewClientService } from 'src/app/interview/interview-client.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  columns=[
    {columnDef: 'id', headerText: 'ID', displayProperty: 'id'},
    {columnDef: 'description', headerText: 'Description', displayProperty: 'description'},
    {columnDef: 'candidateId', headerText: 'Candidate ID', displayProperty: 'candidateId'},
    {columnDef: 'templateId', headerText: 'Template ID', displayProperty: 'templateId'},
];
urlPrefix = 'interview';

client;

constructor(client: InterviewClientService) {
    this.client = client;
 }

ngOnInit(): void {
}


}
