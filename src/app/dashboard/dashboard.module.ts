import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ListComponent } from './list/list.component';
import { GenericsModule } from '../generics/generics.module';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [DashboardComponent, ListComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    GenericsModule,
    CoreModule
  ]
})
export class DashboardModule { }
