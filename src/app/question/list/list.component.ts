import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../question.service';


@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    columns=[
        {columnDef: 'id', headerText: 'Id', displayProperty: 'id'},
        {columnDef: 'questionName', headerText: 'Question', displayProperty: 'questionName'},
        {columnDef: 'subarea', headerText: 'Subarea', displayProperty: 'subarea'},
        {columnDef: 'band', headerText: 'Band', displayProperty: 'band'}
    ];
    urlPrefix = 'questions';

    client;

    constructor(client: QuestionService) {
        this.client = client;
     }
    
    ngOnInit(): void {

    }

}
