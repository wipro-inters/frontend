import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../question.service';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { sprintf } from 'sprintf-js';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  responseId: number; 
    forms = this.fb.group({
        questionName: ['', Validators.required],
        answer: ['', Validators.required],
        questionWeight: ['', Validators.required],
        subareaId: ['', Validators.required],
        bandId: ['', Validators.required]
    });

  constructor(private questionService: QuestionService, 
    private snackbarCreate: SnackbarCreateService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void{
    console.info("Submit", this.forms.value);
    this.questionService.create(this.forms.value).
      subscribe(response => {
        let path = sprintf('/questions/%s', response.id);
        this.snackbarCreate.alertCreated([path], response.id, "Question");
      });
      this.forms.reset();
  }

}
