import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { QuestionComponent } from './question.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  {path: 'questions/create', component: CreateComponent},
  {path: 'questions', component: QuestionComponent},
  {path: 'questions/:id', component: DetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionRoutingModule { }
