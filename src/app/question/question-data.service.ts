import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { QuestionService } from './question.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionDataService {

    private subject = new BehaviorSubject([]);
    
    questions = this.subject.asObservable();

    constructor(private client: QuestionService) {
        this.updateQuestions();
    }

    updateQuestions(): void{
        this.client.list().subscribe(questions => this.setQuestions(questions))
    }

    getQuestions(id: number){
        return this.questions
            .pipe(map(questions => questions.find(question => question.id === id)));
    } 

    setQuestions(questions){
        this.subject.next(questions);
    }

}
