import { Input, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { QuestionService } from '../question.service';
import { MatPaginator } from '@angular/material/paginator';

export interface Question {
    questionName: string;
    id: number;
    answer: string;
    questionWeight: number;
    bandId: number;
    subareaId: number;
}

@Component({
    selector: 'app-question-table',
    templateUrl: './question-table.component.html',
    styleUrls: ['./question-table.component.css']
})
export class QuestionTableComponent implements OnInit {

    @Input() displayedColumns = ['id', 'questionName', 'subarea', 'band', 'detail'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    questions: [];
    selected = [];

    dataSource = new MatTableDataSource<any>(this.questions);
    selection = new SelectionModel<any>(true, []);

    constructor(private service: QuestionService) { }
    

    ngOnInit(): void {
        this.service.list().subscribe(response => this.questions = response.content) 
        this.dataSource.paginator = this.paginator;
    }

    
    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: Question): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.questions.forEach(row => this.selection.select(row));
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.questions.length;
        return numSelected === numRows;
    }

    getSelected(): any{
        return this.questions.filter(row => this.selection.isSelected(row));
    }
    
    onFilterChange(filters): void {
        console.log(filters);
        this.service.filter(filters).subscribe(response => this.questions = response.content);
    }

    remove(id): void {

    }

}
