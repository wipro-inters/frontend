import { Output, EventEmitter, Component, OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-table-filters',
  templateUrl: './table-filters.component.html',
  styleUrls: ['./table-filters.component.css']
})
export class TableFiltersComponent implements OnInit {

    @Output() public filterChange = new EventEmitter()
    private filters = {subareaId: '', areaId: '', bandId: ''};

    constructor() { }

    ngOnInit(): void {
    }

    onBandChange(event): void{
        this.filters.bandId = event.id;
        this.filterChange.emit(this.filters);
    }

    onAreaChange(event): void{
        this.filters.areaId = event.id;
        this.filterChange.emit(this.filters);
    }

    onSubareaChange(event): void {
        this.filters.subareaId = event.id;
        this.filterChange.emit(this.filters);
    }
}
