import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionRoutingModule } from './question-routing.module';
import { QuestionComponent } from './question.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { CoreModule } from '../core/core.module';
import { MatSelectModule } from '@angular/material/select';
import { QuestionDataTableComponent } from './question-data-table/question-data-table.component';
import { BandModule } from '../band/band.module';
import { SubareaModule } from '../subarea/subarea.module';
import { AreaModule } from '../area/area.module';
import { QuestionTableComponent } from './question-table/question-table.component';
import { TableFiltersComponent } from './question-table/table-filters/table-filters.component';
import { DetailComponent } from './detail/detail.component';
import { GenericsModule } from '../generics/generics.module';

@NgModule({
  declarations: [QuestionComponent, CreateComponent, ListComponent, QuestionDataTableComponent, QuestionTableComponent, TableFiltersComponent, DetailComponent],
  imports: [
    CommonModule,
    BandModule,
    QuestionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    CoreModule,
    MatSelectModule,
    BandModule,
    SubareaModule,
    AreaModule,
    GenericsModule
  ],
  exports: [
    QuestionComponent,
    QuestionDataTableComponent
  ]
})
export class QuestionModule { }
