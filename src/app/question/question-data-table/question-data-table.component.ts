import { OnInit, Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-question-data-table',
  templateUrl: './question-data-table.component.html',
  styleUrls: ['./question-data-table.component.css']
})
export class QuestionDataTableComponent implements OnInit {

    @ViewChild('questionsTable') questionsTable;

    columns = ['select', 'questionName', 'band', 'subarea', 'detail']

    constructor() { }


    ngOnInit(): void {
    }

    getSelected(): any{
        return this.questionsTable.getSelected();
    }

}
