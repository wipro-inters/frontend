import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionDataTableComponent } from './question-data-table.component';

describe('QuestionDataTableComponent', () => {
  let component: QuestionDataTableComponent;
  let fixture: ComponentFixture<QuestionDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
