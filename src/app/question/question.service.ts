import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { question_detail, question_create, question_list} from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
    providedIn: 'root',
})
export class QuestionService {

    constructor(private http: HttpClient) { }

    create(question): Observable<any>{
        return this.http.post(question_create, question);
    }

    get(id: string): Observable<any> {
        let endpoint = sprintf(question_detail, id);
        return this.http.get(endpoint);
    }

    list(): Observable<any> {
        return this.http.get(question_list);
    }

    filter(filter): Observable<any> {
        return this.http.get(question_list, {params: filter});
    }

    paging(params): Observable<any>{
      return this.http.get(question_list, {params: params});
    }

}
