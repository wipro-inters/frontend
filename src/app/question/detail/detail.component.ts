import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { QuestionService } from 'src/app/question/question.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  forms: FormGroup;
  id;

  constructor(private questionService: QuestionService,
              private fb: FormBuilder,
              private route: ActivatedRoute,) {
      let questionForm = fb.control({value: '', disabled: true});
      let answerForm = fb.control({value: '', disabled: true});
      let questionWeightForm = fb.control({value: '', disabled: true});
      let bandIdForm = fb.control({value: '', disabled: true});
      let subareaIdForm = fb.control({value: '', disabled: true});
      this.forms = new FormGroup({
          questionName: questionForm,
          answer: answerForm,
          questionWeight: questionWeightForm,
          bandId: bandIdForm,
          subareaId: subareaIdForm,
      });
  }

  ngOnInit(): void {
      let request = () => {
          this.questionService.get(this.id)
              .subscribe(response => this.forms.setValue(response));
      };
      this.route.paramMap.subscribe(params => {
          this.id = params.get('id');
          request();
      });

  }

}
