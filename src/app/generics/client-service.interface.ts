import { Observable } from 'rxjs';

export interface ClientService {
    delete(id: string | number): Observable<any>;
    list(): Observable<any>;
    paging(params): Observable<any>;
}
