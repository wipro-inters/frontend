import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../core/core.module';

import { DataTableComponent } from './data-table/data-table.component';
import { PageableDataTableComponent } from './pageable-data-table/pageable-data-table.component';

@NgModule({
    declarations: [DataTableComponent, PageableDataTableComponent],
    imports: [
        CommonModule,
        CoreModule,
    ],
    exports: [
        DataTableComponent,
        PageableDataTableComponent,
    ],
})
export class GenericsModule { }
