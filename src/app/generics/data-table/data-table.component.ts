import {Input, Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClientService} from '../client-service.interface';

export interface TableColumn {
    columnDef: string;
    headerText: string;
    displayProperty: string;
}

@Component({
  selector: 'app-generic-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

    @Input() columns: TableColumn[] = [];
    @Input() urlPrefix: string = "";
    @Input() editColumn: boolean = false;
    @Input() deleteColumn: boolean = false;
    @Input() detailColumn: boolean = false;
    @Input() selectColumn: boolean = false;
    @Input() clientService: ClientService = null;

    dataSource = [];
    selected = [];
    selection = new SelectionModel<any>(true, []);
    displayedColumns = [];
    editUrlBuilder = row => this.router.createUrlTree([this.urlPrefix, row.id, 'edit']);
    detailUrlBuilder = row => this.router.createUrlTree([this.urlPrefix, row.id]);

    constructor(private router: Router,
                private snackbar: MatSnackBar) { }

    ngOnInit(): void {
        this.clientService.list().subscribe(response => this.dataSource = response);
        this.displayedColumns = this.columns.map(row => row.columnDef);
        if (this.selectColumn)
            this.displayedColumns = ["select", ...this.displayedColumns];
        if (this.editColumn)
            this.displayedColumns.push("edit");
        if (this.detailColumn)
            this.displayedColumns.push("detail");
        if (this.deleteColumn)
            this.displayedColumns.push("delete");
        console.log(this.columns);
        console.log(this.displayedColumns);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.forEach(row => this.selection.select(row));
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.length;
        return numSelected === numRows;
    }

    getSelected(): any{
        return this.dataSource.filter(row => this.selection.isSelected(row));
    }

    deleteFunction(row): void {
        this.clientService.delete(row.id).subscribe( response => {
            this.snackbar.open("Removed", "Close");
            this.clientService.list().subscribe(response => this.dataSource = response);
        });
    }

}
