import { Component, OnInit, Input } from '@angular/core';
import { ClientService } from '../client-service.interface';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface TableColumn {
    columnDef: string;
    headerText: string;
    displayProperty: string;
}

@Component({
  selector: 'app-pageable-data-table',
  templateUrl: './pageable-data-table.component.html',
  styleUrls: ['./pageable-data-table.component.css']
})
export class PageableDataTableComponent implements OnInit {

  @Input() columns: TableColumn[] = [];
  @Input() urlPrefix: string = "";
  @Input() editColumn: boolean = false;
  @Input() deleteColumn: boolean = false;
  @Input() detailColumn: boolean = false;
  @Input() selectColumn: boolean = false;
  @Input() clientService: ClientService = null;
  @Input() pageSize: number = 10;

  dataSource = [];
  selected = [];
  selection = new SelectionModel<any>(true, []);
  displayedColumns = [];
  editUrlBuilder = row => this.router.createUrlTree([this.urlPrefix, row.id, 'edit']);
  detailUrlBuilder = row => this.router.createUrlTree([this.urlPrefix, row.id]);
  length : number;

  constructor(private router: Router,
              private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    const params = {page: 0, numResults: this.pageSize};
      this.clientService.paging(params).subscribe(response => {
        this.dataSource = response.content;
        this.length = response.totalElements;
      });
      this.displayedColumns = this.columns.map(row => row.columnDef);
      if (this.selectColumn)
          this.displayedColumns = ["select", ...this.displayedColumns];
      if (this.editColumn)
          this.displayedColumns.push("edit");
      if (this.detailColumn)
          this.displayedColumns.push("detail");
      if (this.deleteColumn)
          this.displayedColumns.push("delete");
      console.log(this.columns);
      console.log(this.displayedColumns);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?): string {
      if (!row) {
          return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.forEach(row => this.selection.select(row));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.length;
      return numSelected === numRows;
  }

  getSelected(): any{
      return this.dataSource.filter(row => this.selection.isSelected(row));
  }

  deleteFunction(row): void {
      this.clientService.delete(row.id).subscribe( response => {
          this.snackbar.open("Removed", "Close");
          this.clientService.list().subscribe(response => this.dataSource = response);
      });
  }

  pageHandler(event){
    const params = {page: event.pageIndex, numResults: this.pageSize};
    this.clientService.paging(params).subscribe(result => this.dataSource = result.content);
  }


}
