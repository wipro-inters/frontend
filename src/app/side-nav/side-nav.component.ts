import { Component, OnInit, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.css']
})

export class SideNavComponent implements OnInit {
    opened = true;
    language;
    selected;

    private widthClosed = 4;
    private widthOpened = 15;
    sidenavWidth = this.widthOpened;

    @ViewChild('sidenav') sidenav;

    languages = {
        en: 'flag-icon flag-icon-us',
        pt: 'flag-icon flag-icon-br'
    }

    constructor(public translate: TranslateService){
        translate.addLangs(Object.keys(this.languages));
        translate.setDefaultLang('en');
        this.language = this.languages.en;
    }

    switchLang(lang: string) {
        this.language = this.languages[lang]
        this.translate.use(lang);
    }

    ngOnInit(): void {

    }

    sidenav_toggle(): void{
        let sidenavContent = document.getElementById("sidenav_content") as HTMLElement;
        if (this.opened) {
            this.sidenavWidth = this.widthClosed;
            this.setNavItemsDisplay(true);
            sidenavContent.style.marginLeft = '55px';
            this.opened = false;
        }
        else {
            this.sidenavWidth = this.widthOpened;
            sidenavContent.style.marginLeft = '209px';
            this.setNavItemsDisplay(false);
            this.opened = true;
        }
    }

    setNavItemsDisplay(hide: boolean): void{
        let spans = document.getElementsByClassName('navigation-item-label');
        let homeBtn = document.getElementsByClassName('homeBtn')[0] as HTMLElement;
        let languageDiv = document.getElementById('languageContainer') as HTMLElement;
        let value = hide ? 'none' : 'initial';
        Array.from(spans as HTMLCollectionOf<HTMLElement>).forEach(s => {
            s.style.display = value;
        });
        languageDiv.style.display = value;
        homeBtn.style.display = value;
    }

}
