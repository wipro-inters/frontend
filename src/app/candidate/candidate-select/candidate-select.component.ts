import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CandidateService } from '../candidate.service';

@Component({
  selector: 'app-candidate-select',
  templateUrl: './candidate-select.component.html',
  styleUrls: ['./candidate-select.component.css']
})
export class CandidateSelectComponent implements OnInit {

  @Input() params = {}
  @Input() clearField: boolean = false;

  candidates = [];
  selectedCandidate;

  @Output() public candidateChange = new EventEmitter();

  constructor(private service: CandidateService) { }

  ngOnInit(): void {
      this.service.list(this.params)
	  .subscribe(candidates => {
	      if (this.clearField)
		  this.candidates = [{}, ...candidates];
	      else
		  this.candidates = candidates;
	  });
  }

  onSelectionChange(event): void {
      this.candidateChange.emit(this.selectedCandidate);
  }


}
