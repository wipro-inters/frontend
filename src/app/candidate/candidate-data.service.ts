import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CandidateService } from './candidate.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })

export class CandidateDataService {
    
    private subject = new BehaviorSubject([]);
    
    candidates = this.subject.asObservable();

    constructor(private client: CandidateService) {
        this.updateCandidates();
    }

    updateCandidates(): void{
        this.client.list().subscribe(areas => this.setCandidates(areas))
    }

    getCandidate(id: number){
        return this.candidates
            .pipe(map(candidates => candidates.find(candidate => candidate.id === id)));
    } 

    setCandidates(candidates){
        this.subject.next(candidates);
    }
}
