import { Component, OnInit } from '@angular/core';
import { CandidateService } from '../candidate.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CandidateDataService } from '../candidate-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    columns=[
        {columnDef: 'id', headerText: 'Id', displayProperty: 'id'},
    ];
    urlPrefix = 'candidates';

    client;

    constructor(client: CandidateService) {
        this.client = client;
    }

  ngOnInit(): void {
  }


}
