import { Injectable } from '@angular/core';
import { candidate_create, candidate_detail, candidate_list, candidate_update, candidate_delete } from '../endpoints';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { sprintf } from 'sprintf-js';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  constructor(private http: HttpClient) { }

  create(candidate): Observable<any>{
    return this.http.post(candidate_create, candidate);
  }

  get(id: string): Observable<any> {
    let endpoint = sprintf(candidate_detail, id);
    return this.http.get(endpoint);
  }

  list(params = {}): Observable<any> {
    return this.http.get<any>(candidate_list, {params: params})
    .pipe(map(response => response.content));
  }

  update(id, body): Observable<any> {
    let endpoint = sprintf(candidate_update, id);
    return this.http.put(endpoint, body);
  }

  delete(id): Observable<any> {
    let endpoint = sprintf(candidate_delete, id);
    return this.http.delete(endpoint);
  }

    paging(params): Observable<any> {
        return this.http.get<any>(candidate_list, {params: params});
    }
}
