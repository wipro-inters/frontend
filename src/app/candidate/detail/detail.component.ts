import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CandidateService } from '../candidate.service';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

    forms: FormGroup;
    id;
    
    constructor(private candidateService: CandidateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,) {
        let nameForm = fb.control({value: '', disabled: true});
        let emailForm = fb.control({value: '', disabled: true});
        let graduationForm = fb.control({value: '', disabled: true});
        let birthdateForm = fb.control({value: '', disabled: true});
        this.forms = new FormGroup({
            fullName: nameForm,
            email: emailForm,
            graduation: graduationForm,
            birthDate: birthdateForm,
        });
    }



    ngOnInit(): void {
        let request = () => {
            this.candidateService.get(this.id)
                .subscribe(response => this.forms.setValue(response));
        };
        this.route.paramMap.subscribe(params => {
            this.id = params.get('id');
            request();
        });

    }
}
