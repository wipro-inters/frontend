import { NgModule } from  '@angular/core';
import { Routes, RouterModule } from  '@angular/router';

import {CandidateComponent} from './candidate.component';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';

export const routes: Routes = [
    {path: 'candidates/:id/edit', component: EditComponent},
    {path: 'candidates/create', component: CreateComponent},
    {path: 'candidates/:id', component: DetailComponent},
    {path: 'candidates', component: CandidateComponent},
    {path: 'candidates/**', component: CandidateComponent},
    {path: 'candidates/list', component: ListComponent}
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export  class  CandidateRouting { }
