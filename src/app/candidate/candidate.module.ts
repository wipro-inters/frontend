import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { GenericsModule } from '../generics/generics.module';

import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { CandidateComponent } from './candidate.component';

import { CandidateRouting } from './candidate.routing';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';
import { CandidateSelectComponent } from './candidate-select/candidate-select.component';

@NgModule({
    declarations: [
        DetailComponent,
        EditComponent,
        CreateComponent,
        CandidateComponent,
        DeleteComponent,
        ListComponent,
        CandidateSelectComponent
    ],
  imports: [
      CoreModule,
      CandidateRouting,
      GenericsModule,
  ],
    exports: [CandidateComponent, CandidateSelectComponent],
})
export class CandidateModule { }
