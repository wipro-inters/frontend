import { CandidateService } from '../candidate.service';
import { Component, OnInit } from '@angular/core';
import { SnackbarCreateService } from '../../snackbar-create/snackbar-create.service';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { sprintf } from 'sprintf-js';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  opened=true;
  responseId: number; 
  forms = this.fb.group({
      fullName: ['', Validators.required],
      birthDate: ['', Validators.required],
      email: ['', Validators.required],
      graduation: ['', Validators.required]
  });

  constructor(private candidateService: CandidateService,
                private snackbarCreate : SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.info('Submit: ', this.forms.value);
    this.candidateService.create(this.forms.value).
        subscribe(response => {
            let path = sprintf('/candidates/%s', response.id);
            this.snackbarCreate.alertCreated([path], response.id, "Candidate");
        });
    this.forms.reset()
  }
}
