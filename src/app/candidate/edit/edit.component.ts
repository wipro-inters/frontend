import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CandidateService } from '../candidate.service';
import { CandidateDataService } from '../candidate-data.service';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
    forms: FormGroup;
    id;

    constructor(private service: CandidateService,
                private data: CandidateDataService,
                private snackbarService: SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private snackbar: MatSnackBar) {
        let nameForm = fb.control({value: '', disabled: false}, Validators.required);
        let birthDateForm = fb.control({value: '', disabled: false}, Validators.required);
        let emailForm = fb.control({value: '', disabled: false}, Validators.required);
        let graduationForm = fb.control({value: '', disabled: false}, Validators.required);
        this.forms = new FormGroup({
          fullName: nameForm,
          birthDate: birthDateForm,
          email: emailForm,
          graduation: graduationForm, 
        });
    }

    ngOnInit(): void {
      let request = () => {
        this.data.getCandidate(Number(this.id))
            .subscribe(response => this.forms.setValue(response));
    };
    
    this.route.paramMap.subscribe(params => {
        console.log(params);
        this.id = params.get('id');
        request();
    });

    }

    onSubmit(): void {
      this.service.update(this.id, this.forms.value)
          .subscribe(response => {
      this.snackbar.open('Updated', 'Close', {duration: 3000});
      this.router.navigate(['candidates'], {relativeTo: this.route});
    });
}

    delete(): void {
      this.service.delete(this.id).subscribe(response => {
      this.snackbar.open('Deleted', 'Close', {duration: 3000});
      this.router.navigate(['../../'], {relativeTo: this.route});
    });
}

}
