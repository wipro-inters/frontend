import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserService } from './user.service';
import { map } from 'rxjs/operators';
import { sprintf } from 'sprintf-js';
import { users_detail } from '../endpoints';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

    private subject = new BehaviorSubject([]);

    users = this.subject.asObservable();

    constructor(private client: UserService, private http: HttpClient) {
        this.update();
    }

    update(): void{
        this.client.list().subscribe(users => this.set(users))
    }

    get(id: number){
        let endpoint = sprintf(users_detail, id);
        return this.http.get(endpoint);
      }  
  

    set(users){
        this.subject.next(users);
    }

}
