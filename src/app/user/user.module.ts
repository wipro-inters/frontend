import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';
import { CoreModule } from '../core/core.module';
import { ListComponent } from './list/list.component';
import { UserComponent } from './user.component';
import { EditComponent } from './edit/edit.component';
import { RoleModule } from '../role/role.module';
import { GenericsModule } from '../generics/generics.module';

@NgModule({
  declarations: [CreateComponent, DetailComponent, ListComponent, UserComponent, EditComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    CoreModule,
    RoleModule,
    GenericsModule
  ],
  exports: [
    UserComponent
  ]
})
export class UserModule { }
