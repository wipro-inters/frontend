import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  forms: FormGroup;
  id;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
      let idForm = fb.control({value: '', disabled:true });
      let nameForm = fb.control({value: '', disabled: true}, Validators.required);
      let lastNameForm = fb.control({value: '', disabled: true}, Validators.required);
      let birthdateForm = fb.control({value: '', disabled: true}, Validators.required);
      let emailForm = fb.control({value: '', disabled: true}, Validators.required);
      let userNameForm = fb.control({value: '', disabled: true}, Validators.required);
      let roleNameForm = fb.control({value: '', disabled: true}, Validators.required);
      this.forms = new FormGroup({
          id: idForm,
          firstName: nameForm,
          lastName: lastNameForm,
          birthDate: birthdateForm,
          email: emailForm,
          roleName: roleNameForm,
          userName: userNameForm,
      });
  }

  ngOnInit(): void {
    let request = () => {
      this.userService.get(this.id)
          .subscribe(response => this.forms.setValue(response));
    };
    this.route.paramMap.subscribe(params => {
        this.id = params.get('id');
        request();
    });
  }

}
