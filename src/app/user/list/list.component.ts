import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    columns=[
        {columnDef: 'id', headerText: 'Id', displayProperty: 'id'},
        {columnDef: 'firstName', headerText: 'firstName', displayProperty: 'firstName'},
        {columnDef: 'lastName', headerText: 'lastName', displayProperty: 'lastName'}
    ];
    urlPrefix = 'users';

    client;

    constructor(client: UserService) {
        this.client = client;
     }

    ngOnInit(): void {
    }

}
