import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { users_delete, users_update, users_create, users_list, users_detail } from '../endpoints';
import { sprintf } from 'sprintf-js';
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root',
})

export class UserService {

    constructor(private http: HttpClient) { }

    create(user): Observable<any>{
        return this.http.post(users_create, user);
    }

    get(id: string): Observable<any> {
        let endpoint = sprintf(users_detail, id);
        return this.http.get(endpoint);
    }
   
    list(): Observable<any> {
        return this.http.get<any>(users_list)
            .pipe(map(response => response.content));
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(users_update, id);
        return this.http.put(endpoint, body);
    }
    
    delete(id): Observable<any> {
        let endpoint = sprintf(users_delete, id);
        return this.http.delete(endpoint);
    }

    paging(params): Observable<any> {
        return this.http.get<any>(users_list, {params: params});
    }

}
