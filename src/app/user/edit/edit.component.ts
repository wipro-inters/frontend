import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDataService } from '../user-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  @ViewChild("selector")selector;

  forms: FormGroup;
  id;
  user;
  selectedRole;

  constructor(private data: UserDataService,
              private client: UserService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private snackbar: MatSnackBar) 
  {
    let idForm = fb.control({value: '', disabled:true });
    let userNameForm = fb.control({value: '', disabled: false}, Validators.required);
    let firstNameForm = fb.control({value: '', disabled: false}, Validators.required);
    let lastNameForm = fb.control({value: '', disabled: false}, Validators.required);
    let emailForm = fb.control({value: '', disabled: false}, Validators.required);
    let birthDateForm = fb.control({value: '', disabled: false}, Validators.required);
    let roleForm = fb.control({value: '', disabled: false}, Validators.required);
    this.forms = new FormGroup({
      id: idForm,
      userName: userNameForm,
      firstName: firstNameForm,
      lastName: lastNameForm,
      email: emailForm,
      birthDate: birthDateForm,
      roleName: roleForm
    });
  }

  ngOnInit(): void {
    let request = () => {
      this.data.get(this.id)
          .subscribe(response => this.forms.setValue(response));
      this.data.get(this.id).subscribe(response => this.user=response);
    };

    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      request();
    });
  }

  selectionChangeHandler(event){
    this.selectedRole = event;
  }

  onSubmit(): void {
    let payload = this.forms.value;
    payload['roleId'] = this.selectedRole.id;
    
    this.client.update(this.id, payload)
        .subscribe(response => {
          this.snackbar.open('Updated', 'Close', {duration: 3000});
          this.router.navigate(['../'], {relativeTo: this.route});
        });
  }

  delete(): void {
    this.client.delete(this.id).subscribe(response => {
    this.snackbar.open('Deleted', "Close", {duration: 3000});
    this.router.navigate(['../../'], {relativeTo: this.route});
    });
  }
 
}
