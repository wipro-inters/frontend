import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { UserService } from '../user.service';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { ActivatedRoute, Router } from '@angular/router';
import { sprintf } from 'sprintf-js';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  responseId: number; 
  forms = this.fb.group({
      userName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthDate: ['', Validators.required],
      email: ['', Validators.required],
      roleId: ['', Validators.required]
  });

  constructor(private service: UserService,
                private snackbarCreate : SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.info('Submit: ', this.forms.value);
    this.service.create(this.forms.value).
        subscribe(response => {
            let path = sprintf('/users/%s', response.id);
            this.snackbarCreate.alertCreated([path], response.id, "User");
        });
    this.forms.reset()
  }

}
