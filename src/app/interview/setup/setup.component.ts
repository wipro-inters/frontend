import {Output, EventEmitter, Component, OnInit } from '@angular/core';

import { InterviewDataService } from '../interview-data.service';

export interface SetupData {
    candidate: string;
    template: string;
    position: string;
}

@Component({
    selector: 'app-setup',
    templateUrl: './setup.component.html',
    styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {

    private data: SetupData = {} as SetupData;

    params;
    completed;
    hasError;

    constructor(private dataService: InterviewDataService) { }

    ngOnInit(): void {
    }

    changeHandler(attr, event) {
	this.data[attr] = event;
	let violations = Object.entries(this.data).filter(kv => kv[1] === undefined);
	this.completed = violations.length == 0;
	this.hasError = !this.completed;
    }

    getData() {
        return this.data;
    }

}
