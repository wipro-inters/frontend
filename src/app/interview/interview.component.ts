import { ViewChild, Component, OnInit } from '@angular/core';

import { InterviewDataService } from './interview-data.service';

import { MatDialogRef, MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-interview',
    templateUrl: './interview.component.html',
    styleUrls: ['./interview.component.css']
})
export class InterviewComponent implements OnInit {

    @ViewChild("stepper") stepper;

    @ViewChild("setup") setup;
    @ViewChild("evaluation") evaluation;
    @ViewChild("observations") observations;
    @ViewChild("review") review;
    @ViewChild("conclusion") conclusion;
   
    data = {
        scores: [],
        setup: {
            candidate: {fullName: '', id: ''},
            position: {name: '', id: ''},
            template: {name: '', id: ''},
        },
        observations: '',
    };

    isEditable = true;

    constructor(private dataService: InterviewDataService,
                private dialogService: MatDialog) { }

    ngOnInit(): void {
    }


    submit() {
        this.dataService.createInterview(this.getCreateDto());
        this.isEditable = false;
        this.stepper.next();
    }

    getCreateDto() {
        return {
            candidateId: this.data.setup.candidate.id,
            templateId: this.data.setup.template.id,
            conclusion: '1500-01-02',
            interviewer: 'the backend should infeer this from api auth',
            notes: this.data.observations,
            scores: this.data.scores,
        }
    }

    setupFinish() {
        this.data['setup'] = this.setup.getData();
        let dialog = this.dialogService.open(SetupConfirmDialog);
        dialog.afterClosed().subscribe(confirm => {
            if (confirm) {
                this.dataService.getTemplate(this.data['setup']['template']['id']);
                this.evaluation.completed = true;//evaluation has no validation
                this.stepper.next();
            }
        });
    }

    evaluationFinish() {
        this.data['scores'] = this.evaluation.getScores();
        this.observations.completed = true; //observations is optional and has no validation
        this.stepper.next();
    }

    observationsFinish() {
        this.data['observations'] = this.observations.getData();
        this.dataService.interviewDataSubject.next(this.data);
        this.review.completed = true
        this.stepper.next();
    }

}

@Component({
    selector: 'setup-confirm',
    templateUrl: 'setup-confirm.html',
})
export class SetupConfirmDialog {

    constructor(public dialogRef: MatDialogRef<SetupConfirmDialog>) { }

    close(confirm) {
        this.dialogRef.close(confirm);
    }

}
