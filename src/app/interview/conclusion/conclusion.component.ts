import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { InterviewDataService } from '../interview-data.service';

@Component({
    selector: 'app-conclusion',
    templateUrl: './conclusion.component.html',
    styleUrls: ['./conclusion.component.css']
})
export class ConclusionComponent implements OnInit {

    forms: FormGroup;

    constructor(private fb: FormBuilder,
                private dataService: InterviewDataService) { 
        let idForm = fb.control({value: '', disabled:true});
        let interviewerForm = fb.control({value: '', disabled: true});
        let candidateForm = fb.control({value: '', disabled: true});
        let positionForm = fb.control({value: '', disabled: true});
        let dateForm = fb.control({value: '', disabled: true});
        this.forms = new FormGroup({
            id: idForm,
            interviewer: interviewerForm,
            candidate: candidateForm,
            position: positionForm,
            date: dateForm
        });
    }

    ngOnInit(): void {
        this.dataService.responseSubject.subscribe(response => {
            console.log(response);
            this.forms['controls'].id.setValue(response.id);
            //this.forms.interviewerForm.setValue(response.interviewer);
            //this.forms.candidateForm.setValue(response.candidate.fullName);
            //this.forms.dateForm.setValue(response.conclusion);
        });
    }

}
