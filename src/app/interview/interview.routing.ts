import { NgModule } from  '@angular/core';
import { Routes, RouterModule } from  '@angular/router';

import {InterviewComponent} from './interview.component';

export const routes: Routes = [
    {path: 'interviews', component: InterviewComponent},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export  class  InterviewRouting { }
