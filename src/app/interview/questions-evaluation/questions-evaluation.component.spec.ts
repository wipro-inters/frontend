import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsEvaluationComponent } from './questions-evaluation.component';

describe('QuestionsEvaluationComponent', () => {
  let component: QuestionsEvaluationComponent;
  let fixture: ComponentFixture<QuestionsEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
