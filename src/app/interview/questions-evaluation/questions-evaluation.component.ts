import {EventEmitter, Output, Input, Component, OnInit, OnDestroy } from '@angular/core';

import {EvaluationDataService} from '../evaluation-data.service';

@Component({
    selector: 'app-questions-evaluation',
    templateUrl: './questions-evaluation.component.html',
    styleUrls: ['./questions-evaluation.component.css']
})
export class QuestionsEvaluationComponent implements OnInit, OnDestroy {

    @Input() questions = [
        {id: 1, question: 'Question body 1', answer: 'answer 1', questionWeight: 2, subarea: 'important', band: 'b2'},
        {id: 2, question: 'Question body 2 ', answer: 'answer 2', questionWeight: 4, subarea: 'important', band: 'b2'},
        {id: 3, question: 'Question body 3', answer: 'answer 3', questionWeight: 1, subarea: 'other', band: 'b1'},
    ];

    columns = ['question', 'weight', 'subarea', 'band', 'score'];

    private scores = {};

    constructor(private dataService: EvaluationDataService) { }

    ngOnInit(): void {
        this.questions.forEach(question => this.scores[question.id] = 0);
        this.dataService.addComponent(this);
    }

    ngOnDestroy(): void {
        this.dataService.popComponent(this);
    }

    setScore(questionId, event) {
        this.scores[questionId] = event.value;
    }

    public getScores() {
        let scores = [];
        Object.entries(this.scores)
            .forEach( o => scores.push({questionId: o[0], score: o[1]}));
        return scores;
    }
}
