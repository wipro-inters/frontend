import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-observations',
    templateUrl: './observations.component.html',
    styleUrls: ['./observations.component.css']
})
export class ObservationsComponent implements OnInit {

    form = this.fb.group({
	observations: [''],
    });

    completed = false;
    

    constructor(private fb: FormBuilder) { }

    ngOnInit(): void {
    }

    public getData() {
	return this.form.value['observations'];
    }

}
