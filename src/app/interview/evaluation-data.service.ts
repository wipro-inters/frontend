import { Injectable } from '@angular/core';

@Injectable()
export class EvaluationDataService {

    private components = [];

    constructor() { }

    addComponent(component): void {
        this.components.push(component);
    }

    popComponent(component): void {
        this.components = this.components.filter( c => c != component);
    }

    getScores() {
        return this.components.flatMap(c => c.getScores());
    }

}
