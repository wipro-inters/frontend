import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterviewComponent, SetupConfirmDialog } from './interview.component';
import { SetupComponent } from './setup/setup.component';
import { ObservationsComponent } from './observations/observations.component';
import { ReviewComponent } from './review/review.component';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { QuestionsEvaluationComponent } from './questions-evaluation/questions-evaluation.component';
import { ConclusionComponent } from './conclusion/conclusion.component';
import { InterviewRouting } from './interview.routing';

import {CoreModule} from '../core/core.module';

import {MatSliderModule} from '@angular/material/slider';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';

import { CandidateModule } from '../candidate/candidate.module';
import { PositionModule } from '../position/position.module';
import { TemplateModule } from '../template/template.module';

import {EvaluationDataService} from './evaluation-data.service';


@NgModule({
    providers: [
	EvaluationDataService
    ],
    declarations: [
	InterviewComponent,
        SetupConfirmDialog,
	SetupComponent,
	ObservationsComponent,
	ReviewComponent,
	EvaluationComponent,
	QuestionsEvaluationComponent,
	ConclusionComponent,
    ],
    imports: [
        CommonModule,
        CoreModule,
        InterviewRouting,
        MatSliderModule,
        MatTabsModule,
        CandidateModule,
	TemplateModule,
	PositionModule
    ],
    exports: [
        InterviewComponent
    ],
})
export class InterviewModule { }
