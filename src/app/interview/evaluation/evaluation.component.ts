import { Input, Component, OnInit } from '@angular/core';

import { EvaluationDataService } from '../evaluation-data.service';
import { InterviewDataService } from '../interview-data.service';

@Component({
    selector: 'app-evaluation',
    templateUrl: './evaluation.component.html',
    styleUrls: ['./evaluation.component.css']
})
export class EvaluationComponent implements OnInit {

    template;

    groupIcons = {};
    completed = false;

    private unseenIcon = 'visibility_off';
    private seenIcon = 'visibility';
    
    constructor(private interviewDataService: InterviewDataService,
		private evaluationDataService: EvaluationDataService) { }

    ngOnInit(): void {
        this.interviewDataService.templateSubject.subscribe(template => {
            this.template = template;
            this.template.groups.forEach((group, i) => this.groupIcons[i] = this.unseenIcon);
            this.groupIcons[0] = this.seenIcon; //first tab is auto select
        });
    }

    tabChangeEventHandler(event) {
        const currentIcon = this.groupIcons[event.index];
        if (currentIcon === this.unseenIcon) 
            this.groupIcons[event.index] = this.seenIcon;
    }

    getScores() {
        return this.evaluationDataService.getScores();
    }

}
