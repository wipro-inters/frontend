import { Injectable } from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';

import { TemplateService } from '../template/template.service';

import { InterviewClientService } from './interview-client.service';

@Injectable({
    providedIn: 'root'
})
export class InterviewDataService {

    templateSubject = new BehaviorSubject<any>({
	author: '',
	description: '',
	groups: [],
	id: '',
	positionsId: []
    });

    interviewDataSubject = new BehaviorSubject<any>({
        scores: [],
        setup: {
            candidate: {fullName: ''},
            position: {name: ''},
            template: {name: ''},
        },
        observations: '',
    });

    responseSubject = new Subject<any>();

    constructor(private templateService: TemplateService,
                private client: InterviewClientService) { }

    getTemplate(id) {
	this.templateService.get(id)
	    .subscribe(response => {
                console.log(response);
                this.templateSubject.next(response)
            });
    }

    createInterview(data) {
        this.client.post(data)
            .subscribe(response => this.responseSubject.next(response));
    }

}
