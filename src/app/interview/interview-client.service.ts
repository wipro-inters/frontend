import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { sprintf } from 'sprintf-js';
import { map } from 'rxjs/operators';

import { interview_create, interview_list, interview_update, interview_detail } from '../endpoints';

@Injectable({
  providedIn: 'root'
})
export class InterviewClientService {

    constructor(private http: HttpClient) { }

    post(data): Observable<any> {
        return this.http.post(interview_create, data);
    }

    paging(params): Observable<any> {
      return this.http.get<any>(interview_list, {params: params});
    }

    get(id: string): Observable<any> {
      let endpoint = sprintf(interview_detail, id);
      return this.http.get(endpoint);
  }

    list(): Observable<any> {
        return this.http.get<any>(interview_list)
            .pipe(map(response => response.content));
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(interview_update, id);
        return this.http.put(endpoint, body);
    }

}

