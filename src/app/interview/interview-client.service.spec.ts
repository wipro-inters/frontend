import { TestBed } from '@angular/core/testing';

import { InterviewClientService } from './interview-client.service';

describe('InterviewClientService', () => {
  let service: InterviewClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterviewClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
