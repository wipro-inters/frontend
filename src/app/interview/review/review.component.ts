import { Input, Component, OnInit } from '@angular/core';

import { InterviewDataService } from '../interview-data.service';

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

    constructor(private dataService: InterviewDataService) { }

    questions = [];
    completed = false;

    columns = ['question', 'score'];

    private template = {groups: []};
    data = {
        scores: [],
        setup: {
            candidate: {fullName: ''},
            position: {name: ''},
            template: {name: ''},
        },
        observations: '',
    };

    ngOnInit(): void {
        this.dataService.templateSubject.subscribe(template => this.template = template);
        this.dataService.interviewDataSubject.subscribe(data => {
            this.data = data;
            if (this.data['scores'].length > 0 && this.template.groups.length > 0)
                this.questions = this.genQuestions();
        });
    }

    genQuestions() {
	const questions = this.template.groups.map(group => group.questions).flat();
        let map = {};
	questions.forEach(q => map[q.id] = {'question': q.question});
	this.data.scores.forEach(score => map[score.questionId]['score'] = score.score);
        let questionData = Object.entries(map).map(o => {
            let question = {
                'question': o[1]['question'],
                'score': o[1]['score']
            }
            return question;
        });
        return questionData;
    }

}
