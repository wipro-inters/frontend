import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackbarCreateComponent } from './snackbar-create.component';

describe('SnackbarCreateComponent', () => {
  let component: SnackbarCreateComponent;
  let fixture: ComponentFixture<SnackbarCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackbarCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackbarCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
