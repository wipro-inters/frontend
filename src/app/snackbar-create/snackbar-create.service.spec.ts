import { TestBed } from '@angular/core/testing';

import { SnackbarCreateService } from './snackbar-create.service';

describe('SnackbarCreateService', () => {
  let service: SnackbarCreateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SnackbarCreateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
