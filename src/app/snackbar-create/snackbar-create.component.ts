import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-snackbar-create',
  templateUrl: './snackbar-create.component.html',
  styleUrls: ['./snackbar-create.component.css']
})
export class SnackbarCreateComponent {
    routerPath: any[];
    returnedId: number;
    entity: string;
    router: Router;
    data: any;
    snackBar: MatSnackBar;

    constructor(router: Router,
                @Inject(MAT_SNACK_BAR_DATA) data: any,
                snackBar: MatSnackBar) {
        this.snackBar = snackBar;
        this.router = router;
        this.data = data;
        this.routerPath = data.routerPath;
        this.returnedId = data.returnedId;
        this.entity = data.entity;
    }

    onClick() {
        this.router.navigate(this.routerPath);
        this.snackBar.dismiss();
    }

}
