import { Injectable } from '@angular/core';
import { SnackbarCreateComponent } from './snackbar-create.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarCreateService {

    constructor(private snackBar: MatSnackBar) { }

    alertCreated(routerPath: any[], returnedId: number, entity: string){
        let data = {routerPath: routerPath, returnedId: returnedId, entity: entity};
        this.snackBar.openFromComponent(SnackbarCreateComponent, {duration: 5000, data: data});
    }
}
