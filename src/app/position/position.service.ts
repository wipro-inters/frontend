import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { position_delete, position_update, position_create, position_list, position_detail } from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
    providedIn: 'root',
})
export class PositionService {

    constructor(private http: HttpClient) { }

    create(position): Observable<any>{
        return this.http.post(position_create, position);
    }

    get(id: string): Observable<any> {
        let endpoint = sprintf(position_detail, id);
        return this.http.get(endpoint);
    }

    list(params = {}): Observable<any> {
        return this.http.get(position_list, {params: params});
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(position_update, id);
        return this.http.put(endpoint, body);
    }
    
    delete(id): Observable<any> {
        let endpoint = sprintf(position_delete, id);
        return this.http.delete(endpoint);
    }

}
