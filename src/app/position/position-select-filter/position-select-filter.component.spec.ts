import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionSelectFilterComponent } from './position-select-filter.component';

describe('PositionSelectFilterComponent', () => {
  let component: PositionSelectFilterComponent;
  let fixture: ComponentFixture<PositionSelectFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionSelectFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionSelectFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
