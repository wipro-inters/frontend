import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PositionService } from '../position.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-position-select-filter',
  templateUrl: './position-select-filter.component.html',
})
export class PositionSelectFilterComponent implements OnInit {
  filterValues = {};
  positions = [];
  selected = [];

  dataSource = new MatTableDataSource<any>(this.positions);
  selection = new SelectionModel<any>(true, []);

  filterSelectObj = [];

  constructor(private service: PositionService) {
    this.filterSelectObj = [
      {
      name: 'NAME',
      columnProp: 'name',
      options: []
    }
    ]
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.getRemoteData();
    this.dataSource.filterPredicate = this.createFilter();
  }

  getRemoteData() {
    //this.dataSource.data = this.positions;

    //this.filterSelectObj.filter((o) => {
    //  o.options = this.getFilterObject(this.positions, o.columnProp);
    //});
  }

  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }

   // Called on Filter change
   filterChange(filter, event) {
    //let filterValues = {}
    this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase()
    this.dataSource.filter = JSON.stringify(this.filterValues)
  }

  // Custom filter method fot Angular Material Datatable
  createFilter() {
    let filterFunction = function (data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      let isFilterSet = false;
      for (const col in searchTerms) {
        if (searchTerms[col].toString() !== '') {
          isFilterSet = true;
        } else {
          delete searchTerms[col];
        }
      }

      console.log(searchTerms);

      let nameSearch = () => {
        let found = false;
        if (isFilterSet) {
          for (const col in searchTerms) {
            searchTerms[col].trim().toLowerCase().split(' ').forEach(word => {
              if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                found = true
              }
            });
          }
          return found
        } else {
          return true;
        }
      }
      return nameSearch()
    }
    return filterFunction
  }

  // Reset table filters
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
  }

}
