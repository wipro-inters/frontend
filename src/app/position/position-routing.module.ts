import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { PositionComponent } from './position.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {path: 'positions', component: PositionComponent},
  {path: 'positions/list', component: ListComponent},
  {path: 'positions/create', component: CreateComponent},
  {path: 'positions/:id/edit', component: EditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionRoutingModule { }

