import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { PositionService } from '../position.service';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { ActivatedRoute, Router } from '@angular/router';
import { sprintf } from 'sprintf-js';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  opened = true;
  responseId: number;
  forms = this.fb.group({
    name: ['', Validators.required]
  });

  createResponse;

  constructor(private positionService: PositionService, 
              private snackbarCreate: SnackbarCreateService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    
  }

  onSubmit(): void {
    console.info("Submit", this.forms.value);
    this.positionService.create(this.forms.value).
      subscribe(response => {
        let path = sprintf('/positions/%s', response.id);
        this.snackbarCreate.alertCreated([path], response.id, "Position");
      });
      this.forms.reset();
  }

}
