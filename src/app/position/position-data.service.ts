import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PositionService } from './position.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PositionDataService {

    private subject = new BehaviorSubject([]);
    
    positions = this.subject.asObservable();

    constructor(private client: PositionService) {
        this.updatePositions();
    }

    updatePositions(): void{
        this.client.list().subscribe(positions => this.setPositions(positions))
    }

    getPositions(id: number){
        return this.positions
            .pipe(map(positions => positions.find(position => position.id === id)));
    } 

    setPositions(positions){
        this.subject.next(positions);
    }

}
