import { Component, OnInit } from '@angular/core';

import { PositionService } from '../position.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {

    columns = [
        {columnDef: 'id', headerText: 'Id', displayProperty: 'id'},
        {columnDef: 'name', headerText: 'Name', displayProperty: 'name'},
    ];
    urlPrefix = "/positions";

    constructor(public service: PositionService) { }

    ngOnInit(): void {
    }

}
