import { Component, OnInit, ViewChild } from '@angular/core';
import { PositionService } from '../position.service';

import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';

export interface Position {
  name: string;
  id: number;
}

@Component({
  selector: 'app-position-data-table',
  templateUrl: './position-data-table.component.html',
  styleUrls: ['./position-data-table.component.css']
})

export class PositionDataTableComponent implements OnInit {
  positions = [];
  selected = [];

  displayedColumns: string[] = ['select', 'id', 'name'];
  dataSource = new MatTableDataSource<any>(this.positions);
  selection = new SelectionModel<any>(true, []);

  filterSelectObj = [];

  constructor(private service: PositionService) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.service.list().subscribe(response => this.positions = response)   
    this.dataSource.paginator = this.paginator;
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Position): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.positions.forEach(row => this.selection.select(row));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.positions.length;
    return numSelected === numRows;
  }

getSelected(): any{
  return this.positions.filter(row => this.selection.isSelected(row));
}

}
 
