import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionDataTableComponent } from './position-data-table.component';

describe('PositionDataTableComponent', () => {
  let component: PositionDataTableComponent;
  let fixture: ComponentFixture<PositionDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionDataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
