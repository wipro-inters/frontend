import { Input, Output, EventEmitter, Component, OnInit } from '@angular/core';
import { PositionService } from '../position.service';

@Component({
    selector: 'app-position-select',
    templateUrl: './position-select.component.html',
    styleUrls: ['./position-select.component.css']
})
export class PositionSelectComponent implements OnInit {

    @Input() params = {}
    @Input() clearField: boolean = false;
    @Output() public selectionChange = new EventEmitter();

    selected;
    positions = [];

    constructor(private service: PositionService) { }

    ngOnInit(): void {
	this.service.list(this.params)
	    .subscribe(response => {
		if (this.clearField)
		    this.positions = [{}, ...response]
		else 
		    this.positions = response;
	    });
    }

    onSelectionChange(event) {
	this.selectionChange.emit(this.selected);
    }

}
