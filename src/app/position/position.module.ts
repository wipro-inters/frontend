import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../core/core.module';
import { GenericsModule } from '../generics/generics.module';

import { PositionRoutingModule } from './position-routing.module';
import { PositionComponent } from './position.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';

import { PositionDataTableComponent } from './position-data-table/position-data-table.component';
import { PositionSelectFilterComponent } from './position-select-filter/position-select-filter.component';
import { EditComponent } from './edit/edit.component';
import { PositionSelectComponent } from './position-select/position-select.component';

@NgModule({
  declarations: [PositionComponent, CreateComponent, ListComponent, PositionDataTableComponent, PositionSelectFilterComponent, EditComponent, PositionSelectComponent],
  imports: [
    CommonModule,
    PositionRoutingModule,
    CommonModule,
    CoreModule,
    GenericsModule,
  ],
  exports: [
    PositionComponent,
    PositionDataTableComponent,
    PositionSelectComponent,
  ]
})
export class PositionModule { }
