import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { RoleComponent } from './role.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {path: 'roles/create', component: CreateComponent},
  {path: 'roles', component: RoleComponent},
  {path: 'roles/:id/edit', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
