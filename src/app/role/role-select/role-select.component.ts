import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-role-select',
  templateUrl: './role-select.component.html'
})

export class RoleSelectComponent implements OnInit {

  roles = [];
  selectedRole;

  @Output() public roleChange = new EventEmitter();

  private emptyRole = {id: '', role: ''};

  constructor(private service: ClientService) { }

  ngOnInit(): void {
      this.service.list().subscribe(roles => this.roles = [this.emptyRole, ...roles]);
  }

  onSelectionChange(event): void {
      this.roleChange.emit(this.selectedRole);
  }


}
