import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoleDataService } from '../role-data.service';
import { ClientService } from '../client.service';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  forms: FormGroup;
  id;


  constructor(private data: RoleDataService,
              private client: ClientService,
              private snackbarService: SnackbarCreateService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private snackbar: MatSnackBar) {
      let idForm = fb.control({value: '', disabled:true });
      let nameForm = fb.control({value: '', disabled: false}, Validators.required);
      this.forms = new FormGroup({
          id: idForm,
          name: nameForm,
      });

  }

  ngOnInit(): void {
      let request = () => {
          this.data.getRole(Number(this.id))
              .subscribe(response => this.forms.setValue(response));
      };

      this.route.paramMap.subscribe(params => {
          console.log(params);
          this.id = params.get('id');
          request();
      });

  }

  onSubmit(): void {
      this.client.update(this.id, this.forms.value)
          .subscribe(response => {
              console.log(response);
              this.snackbar.open('Updated', 'Close', {duration: 3000});
              this.router.navigate(['../../'], {relativeTo: this.route});
          });
  }

  delete(): void {
      this.client.delete(this.id).subscribe(response => {
          this.snackbar.open('Deleted', 'Close', {duration: 3000});
          this.router.navigate(['../../'], {relativeTo: this.route});
      });
  }

}
