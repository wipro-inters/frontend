import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  opened=true;
    responseId: number; 
    forms = this.fb.group({
        name: ['', Validators.required],
    });

  constructor(private clientService: ClientService,
              private snackbarCreate : SnackbarCreateService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.info('Submit: ', this.forms.value);
    this.clientService.create(this.forms.value).
        subscribe(response => this.snackbarCreate.alertCreated(["role"], response.id, "Role"));
    this.forms.reset()
  }

}
