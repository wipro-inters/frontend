import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleRoutingModule } from './role-routing.module';
import { RoleComponent } from './role.component';
import { CoreModule } from '../core/core.module';
import { RoleSelectComponent } from './role-select/role-select.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';


@NgModule({
  declarations: [RoleComponent, RoleSelectComponent, ListComponent, EditComponent],
  imports: [
    CommonModule,
    RoleRoutingModule,
    CoreModule
  ],
  exports: [RoleComponent, RoleSelectComponent]
})
export class RoleModule { }
