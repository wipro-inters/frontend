import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RoleDataService } from '../role-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  roles;
  columns = ['id', 'role', 'details', 'edit', 'delete'];
  constructor(private client: ClientService,
              private data: RoleDataService,
              private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.client.list().subscribe(response => this.roles = response)
  }

  remove(id) {
    this.client.delete(id).subscribe( response => {
        this.snackbar.open("Removed", "Close");
        this.client.list().subscribe(roles => this.data.setRole(roles));
    });
}

}
