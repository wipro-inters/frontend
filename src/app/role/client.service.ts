import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { roles_create, roles_list, roles_delete, roles_update } from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
    providedIn: 'root',
})
export class ClientService {

    constructor(private http: HttpClient) { }

    create(role): Observable<any>{
        return this.http.post(roles_create, role);
    }

    list(): Observable<any> {
        return this.http.get<any>(roles_list);
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(roles_update, id);
        return this.http.put(endpoint, body);
    }

    delete(id): Observable<any> {
        let endpoint = sprintf(roles_delete, id);
        return this.http.delete(endpoint);
    }
}
