import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ClientService } from './client.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class RoleDataService {

    private subject = new BehaviorSubject([]);
    
    roles = this.subject.asObservable();

    constructor(private client: ClientService) {
        this.updateRole();
    }

    updateRole(): void{
        this.client.list().subscribe(roles => this.setRole(roles))
    }

    getRole(id: number){
        return this.roles
            .pipe(map(roles => roles.find(role => role.id === id)));
    } 

    setRole(roles){
        this.subject.next(roles);
    }

}
