import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';

import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { AreaComponent } from './area.component';

import { AreaRouting } from './area.routing';
import { AreaSelectComponent } from './area-select/area-select.component';

@NgModule({
    declarations: [
        ListComponent,
        EditComponent,
        CreateComponent,
        AreaComponent,
        AreaSelectComponent
    ],
  imports: [
      CoreModule,
      AreaRouting,
  ],
    exports: [AreaComponent, AreaSelectComponent],
})
export class AreaModule { }
