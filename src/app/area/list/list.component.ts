import { Component, OnInit } from '@angular/core';
import { AreaService } from 'src/app/area/area.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AreasDataService } from '../area-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  areas;
  columns = ['id', 'area', 'edit', 'delete'];
  constructor(private client: AreaService,
              private data: AreasDataService,
              private snackbar: MatSnackBar) { }

  ngOnInit(): void {
      this.data.areas.subscribe(areas => this.areas = areas);
      this.client.list().subscribe(areas => this.data.setAreas(areas));
  }

  remove(id) {
      this.client.delete(id).subscribe( response => {
          this.snackbar.open("Removed", "Close");
          this.client.list().subscribe(areas => this.data.setAreas(areas));
      });
  }

}
