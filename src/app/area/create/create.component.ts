import { FormBuilder, Validators } from '@angular/forms';
import { SnackbarCreateService } from '../../snackbar-create/snackbar-create.service';
import { AreaService } from '../area.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  opened=true;
    responseId: number; 
    forms = this.fb.group({
        name: ['', Validators.required],
    });

  constructor(private areaService: AreaService,
              private snackbarCreate : SnackbarCreateService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.info('Submit: ', this.forms.value);
    this.areaService.create(this.forms.value).
        subscribe(response => this.snackbarCreate.alertCreated(["vacancy"], response.id, "Area"));
    this.forms.reset()
  }

}
