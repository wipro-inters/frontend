import { NgModule } from  '@angular/core';
import { Routes, RouterModule } from  '@angular/router';

import { AreaComponent} from './area.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

export const routes: Routes = [
    {path: 'areas/:id/edit', component: EditComponent},
    {path: 'areas/create', component: CreateComponent},
    {path: 'areas', component: AreaComponent},
    {path: 'areas/**', component: AreaComponent}
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export  class  AreaRouting { }
