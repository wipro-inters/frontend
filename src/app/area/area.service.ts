import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { area_create, area_list, area_delete, area_update } from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  constructor(private http: HttpClient) { }

  create(area): Observable<any>{
      return this.http.post(area_create, area);
  }

  list(): Observable<any> {
      return this.http.get<any>(area_list)
          .pipe(map(response => response.content));
  }

  update(id, body): Observable<any> {
      let endpoint = sprintf(area_update, id);
      return this.http.put(endpoint, body);
  }

  delete(id): Observable<any> {
      let endpoint = sprintf(area_delete, id);
      return this.http.delete(endpoint);
  }
}
