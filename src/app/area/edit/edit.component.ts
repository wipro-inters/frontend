import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackbarCreateService } from '../../snackbar-create/snackbar-create.service';
import { AreaService } from './../area.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AreasDataService } from '../area-data.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  opened=true;
    forms: FormGroup;
    id;

    constructor(private service: AreaService,
                private data: AreasDataService,
                private snackbarService: SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private snackbar: MatSnackBar) {
        let idForm = fb.control({value: '', disabled:true });
        let nameForm = fb.control({value: '', disabled: false}, Validators.required);
        this.forms = new FormGroup({
          id: idForm,
          name: nameForm,
        });
    }

    ngOnInit(): void {
      let request = () => {
        this.data.getArea(Number(this.id))
            .subscribe(response => this.forms.setValue(response));
    };
    
    this.route.paramMap.subscribe(params => {
        console.log(params);
        this.id = params.get('id');
        request();
    });

    }

    onSubmit(): void {
      this.service.update(this.id, this.forms.value)
          .subscribe(response => {
      this.snackbar.open('Updated', 'Close', {duration: 3000});
      this.router.navigate(['../'], {relativeTo: this.route});
    });
}

    delete(): void {
      this.service.delete(this.id).subscribe(response => {
      this.snackbar.open('Deleted', 'Close', {duration: 3000});
      this.router.navigate(['../../'], {relativeTo: this.route});
    });
}


}
