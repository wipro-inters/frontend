import { Output, EventEmitter, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AreaService } from '../area.service';

@Component({
  selector: 'app-area-select',
  templateUrl: './area-select.component.html',
  styleUrls: ['./area-select.component.css']
})
export class AreaSelectComponent implements OnInit {

    areas = [];
    selectedArea;
    private emptyArea = {id: '', area: ''};

    @Output() public areaChange = new EventEmitter();

    constructor(private service: AreaService) { }

  ngOnInit(): void {
      this.service.list().subscribe(areas => this.areas = [this.emptyArea, ...areas]);
  }

    onSelectionChange(event): void {
        this.areaChange.emit(this.selectedArea);
    }

}
