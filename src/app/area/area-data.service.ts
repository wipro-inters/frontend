import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AreaService } from '../area/area.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })

export class AreasDataService {
    
    private subject = new BehaviorSubject([]);
    
    areas = this.subject.asObservable();

    constructor(private client: AreaService) {
        this.updateAreas();
    }

    updateAreas(): void{
        this.client.list().subscribe(areas => this.setAreas(areas))
    }

    getArea(id: number){
        return this.areas
            .pipe(map(areas => areas.find(area => area.id === id)));
    } 

    setAreas(areas){
        this.subject.next(areas);
    }
}
