import { NgModule } from  '@angular/core';
import { Routes, RouterModule } from  '@angular/router';

import {SubareaComponent} from './subarea.component';
import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';

export const routes: Routes = [
    {path: 'subareas/:id/edit', component: EditComponent},
    {path: 'subareas/create', component: CreateComponent},
    {path: 'subareas/:id', component: DetailComponent},
    {path: 'subareas', component: SubareaComponent},
    {path: 'subareas/**', component: SubareaComponent}
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export  class  SubareaRouting { }
