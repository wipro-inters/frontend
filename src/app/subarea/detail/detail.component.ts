import { Component, OnInit } from '@angular/core';
import { SubareasDataService } from '../subareas-data.service';
import { SnackbarCreateService } from '../../snackbar-create/snackbar-create.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

    opened=true;
    forms: FormGroup;
    id;

    constructor(private data: SubareasDataService,
                private snackbarService: SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute) {
        let idForm = fb.control({value: '', disabled:true });
        let nameForm = fb.control({value: '', disabled: true}, Validators.required);
        this.forms = new FormGroup({
            id: idForm,
            name: nameForm,
        });
    }

    ngOnInit(): void {
        let request = () => {
            this.data.getSubarea(Number(this.id))
                .subscribe(response => this.forms.setValue(response));
        };
        request();

        this.route.paramMap.subscribe(params => {
            console.log(params);
            this.id = params.get('id');
            request();
        });
    }
                                     

    onSubmit(): void {
        console.log(this.forms);
    }

}
