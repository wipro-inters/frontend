import { TestBed } from '@angular/core/testing';

import { SubareasDataService } from './subareas-data.service';

describe('SubareasDataService', () => {
  let service: SubareasDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubareasDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
