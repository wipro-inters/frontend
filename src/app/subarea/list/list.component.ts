import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { SubareasDataService } from '../subareas-data.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    subareas;
    columns = ['id', 'name', 'details', 'edit', 'delete'];
    constructor(private client: ClientService,
                private data: SubareasDataService,
                private snackbar: MatSnackBar) { }

    ngOnInit(): void {
        this.data.subareas.subscribe(subareas => this.subareas = subareas);
        this.client.list().subscribe(subareas => this.data.setSubareas(subareas));
    }

    remove(id) {
        this.client.delete(id).subscribe( response => {
            this.snackbar.open("Removed", "Close");
            this.client.list().subscribe(subareas => this.data.setSubareas(subareas));
        });
    }

}
