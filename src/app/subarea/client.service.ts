import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { sub_area_delete, subarea_update, subarea_create, sub_area_list } from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
    providedIn: 'root',
})
export class ClientService {

    constructor(private http: HttpClient) { }

    create(subarea): Observable<any>{
        return this.http.post(subarea_create, subarea);
    }

    list(): Observable<any> {
        return this.http.get<any>(sub_area_list)
            .pipe(map(response => response.content));
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(subarea_update, id);
        return this.http.put(endpoint, body);
    }

    delete(id): Observable<any> {
        let endpoint = sprintf(sub_area_delete, id);
        return this.http.delete(endpoint);
    }
}
