import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubareaSelectComponent } from './subarea-select.component';

describe('SubareaSelectComponent', () => {
  let component: SubareaSelectComponent;
  let fixture: ComponentFixture<SubareaSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubareaSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubareaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
