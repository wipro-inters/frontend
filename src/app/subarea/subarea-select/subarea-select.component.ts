import { EventEmitter, Output, Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';

@Component({
    selector: 'app-subarea-select',
    templateUrl: './subarea-select.component.html',
})
export class SubareaSelectComponent implements OnInit {

    subareas = [];
    selectedSubarea;
    private emptySubarea = {id: '', subarea: ''};

    @Output() public subareaChange = new EventEmitter();

    constructor(private service: ClientService) { }

    ngOnInit(): void {
        this.service.list().subscribe(subareas => this.subareas = [this.emptySubarea, ...subareas]);
    }

    onSelectionChange(event): void {
        this.subareaChange.emit(this.selectedSubarea);
    }

}
