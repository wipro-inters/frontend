import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { SubareaRouting } from './subarea.routing';

import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { SubareaComponent } from './subarea.component';
import { SubareaSelectorComponent } from './subarea-selector/subarea-selector.component';
import { SubareaSelectComponent } from './subarea-select/subarea-select.component';


@NgModule({
    declarations: [
        ListComponent,
        DetailComponent,
        EditComponent,
        CreateComponent,
        SubareaComponent,
        SubareaSelectorComponent,
        SubareaSelectComponent
    ],
  imports: [
      CoreModule,
      SubareaRouting,
      NgMultiSelectDropDownModule,
  ],
    exports: [
        SubareaComponent,
        SubareaSelectComponent,
        SubareaSelectorComponent,
    ],
})
export class SubareaModule { }
