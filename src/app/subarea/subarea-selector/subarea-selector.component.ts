import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SubareasDataService } from '../subareas-data.service';
import { ClientService } from '../client.service';

@Component({
    selector: 'app-subarea-selector',
    templateUrl: './subarea-selector.component.html'
})
export class SubareaSelectorComponent implements OnInit {

    subareas = [];
    selected = [];

    settings: IDropdownSettings = {
        singleSelection: false,
        enableCheckAll: false,
        idField: 'id',
        textField: 'name',
        itemsShowLimit: 3,
        allowSearchFilter: true,
        defaultOpen: false,
    };



    constructor(private client: ClientService,
                private data: SubareasDataService,) { }

    ngOnInit(): void {
        this.client.list()
            .subscribe(subareas => this.data.setSubareas(subareas));
        this.data.subareas.subscribe(subareas => this.subareas = subareas);
    }


    onItemSelect(item: any) {
        
    }

}
