import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubareaSelectorComponent } from './subarea-selector.component';

describe('SubareaSelectorComponent', () => {
  let component: SubareaSelectorComponent;
  let fixture: ComponentFixture<SubareaSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubareaSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubareaSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
