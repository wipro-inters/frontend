import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { SubareasDataService } from '../subareas-data.service';
import { SnackbarCreateService } from '../../snackbar-create/snackbar-create.service';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { sprintf } from 'sprintf-js';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    opened=true;
    responseId: number; 
    forms = this.fb.group({
        name: ['', Validators.required],
    });

    constructor(private subareaService: ClientService,
                private snackbarCreate : SnackbarCreateService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private data: SubareasDataService) { }


    ngOnInit(): void {
    }

    onSubmit(): void {
        console.info('Submit: ', this.forms.value);
        this.subareaService.create(this.forms.value).
            subscribe(response => {
                let path = sprintf('/subareas/%s', response.id);
                this.snackbarCreate.alertCreated([path], response.id, "Subarea");
                this.data.updateSubareas();
            });
        this.forms.reset()
    }

}
