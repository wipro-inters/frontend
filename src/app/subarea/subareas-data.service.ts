import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ClientService } from './client.service';
import { map } from 'rxjs/operators';


/**
 * Service class to share data between unrelated (sibling) components.
 * Makes use of rxjs observables to guarantee data is synced.  
 */
@Injectable({
  providedIn: 'root'
})
export class SubareasDataService {

    private subject = new BehaviorSubject([]);
    
    subareas = this.subject.asObservable();

    constructor(private client: ClientService) {
        this.updateSubareas();
    }

    updateSubareas(): void{
        this.client.list().subscribe(subareas => this.setSubareas(subareas))
    }

    getSubarea(id: number){
        return this.subareas
            .pipe(map(subareas => subareas.find(subarea => subarea.id === id)));
    } 

    setSubareas(subareas){
        this.subject.next(subareas);
    }

}
