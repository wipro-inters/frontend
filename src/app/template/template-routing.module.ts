import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { TemplateComponent } from './template.component';

const routes: Routes = [
  {path: 'templates/:id/edit', component: EditComponent},
    {path: 'templates/create', component: CreateComponent},
    {path: 'templates', component: TemplateComponent},
    {path: 'templates/list', component: ListComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TemplateRoutingModule { }
