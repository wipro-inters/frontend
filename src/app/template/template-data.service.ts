import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TemplateService } from './template.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TemplateDataService {

    private subject = new BehaviorSubject([]);
    
    templates = this.subject.asObservable();

    constructor(private client: TemplateService) {
        this.updateTemplates();
    }

    updateTemplates(): void{
        this.client.list().subscribe(templates => this.setTemplates(templates))
    }

    getTemplate(id: number){
        return this.templates
            .pipe(map(templates => templates.find(template => template.id === id)));
    } 

    setTemplates(templates){
        this.subject.next(templates);
    }

}
