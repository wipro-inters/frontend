import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder} from '@angular/forms';
import { PositionService } from 'src/app/position/position.service';
import { PositionDataService } from 'src/app/position/position-data.service';
import { QuestionService } from 'src/app/question/question.service';
import { QuestionDataService } from 'src/app/question/question-data.service';
import { TemplateService } from '../template.service';
import { sprintf } from 'sprintf-js';
import { SnackbarCreateService } from 'src/app/snackbar-create/snackbar-create.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})

export class CreateComponent implements OnInit {
  @ViewChild('questionDataTable') questionDataTable;
  @ViewChild('positionDataTable') positionDataTable;
  positions = [];
  questions = [];
  selected = [];

  forms = this.fb.group({
    name: ['', Validators.required],
    positionsId: [''],
    questionsId: ['']
  });

  constructor(private templateService: TemplateService,
              private snackbarCreate: SnackbarCreateService,
              private positionClient: PositionService,
              private positionData: PositionDataService,
              private questionData: QuestionDataService,
              private questionClient: QuestionService,
              private fb: FormBuilder,
             ) { }

  ngOnInit(): void {
    this.questionData.questions.subscribe(questions => this.questions = questions);
    this.questionClient.list().subscribe(questions => this.questionData.setQuestions(questions));
    this.positionData.positions.subscribe(positions => this.positions = positions);
    this.positionClient.list().subscribe(positions => this.positionData.setPositions(positions));
  }

  onSubmit(){
    let value = this.forms.value;
    value.positionsId = this.positionDataTable.getSelected().map(row => row['id']);
    value.questionsId = this.questionDataTable.getSelected().map(row => row['id']);
    console.log(value)
    console.info("Submit", this.forms.value);
    this.templateService.create(value).
      subscribe(response => {
        let path = sprintf('/templates/%s', response.id);
        this.snackbarCreate.alertCreated([path], response.id, "Template");
      });
      this.forms.reset();
  }

}
