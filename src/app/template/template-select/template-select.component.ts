import { Input, Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TemplateService } from '../template.service';

@Component({
  selector: 'app-template-select',
  templateUrl: './template-select.component.html',
})
export class TemplateSelectComponent implements OnInit {

  @Input() params = {};
  @Input() clearField: boolean = false;

  templates = [];
  selectedTemplate;

  @Output() public templateChange = new EventEmitter();

  constructor(private service: TemplateService) { }

  ngOnInit(): void {
      this.service.list(this.params)
	  .subscribe(templates => {
	      if (this.clearField)
		  this.templates = [{}, ...templates];
	      else
		  this.templates = templates;
	  });
  }

  onSelectionChange(event): void {
      this.templateChange.emit(this.selectedTemplate);
  }
}
