import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../template.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {

  columns=[
    {columnDef: 'id', headerText: 'Id', displayProperty: 'id'},
    {columnDef: 'name', headerText: 'Name', displayProperty: 'name'}];

urlPrefix = 'templates';

client;

  constructor(client: TemplateService) { 
    this.client = client;
  }
    
  ngOnInit(): void {
  }

}
