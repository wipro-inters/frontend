import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { template_detail, template_delete, template_update, template_create, template_list } from '../endpoints';
import { sprintf } from 'sprintf-js';

@Injectable({
    providedIn: 'root',
})
export class TemplateService {

    constructor(private http: HttpClient) { }

    create(template): Observable<any>{
        return this.http.post(template_create, template);
    }

    get(id: string): Observable<any> {
        let endpoint = sprintf(template_detail, id);
        return this.http.get(endpoint);
    }

    list(params = {}): Observable<any> {
        return this.http.get<any>(template_list, {params: params})
          .pipe(map(response => response.content));
    }

    update(id, body): Observable<any> {
        let endpoint = sprintf(template_update, id);
        return this.http.put(endpoint, body);
    }
    
    delete(id): Observable<any> {
        let endpoint = sprintf(template_delete, id);
        return this.http.delete(endpoint);
    }

    paging(params): Observable<any> {
        return this.http.get<any>(template_list, {params: params});
    }
}
