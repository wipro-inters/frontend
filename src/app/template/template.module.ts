import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { TemplateRoutingModule } from './template-routing.module';
import { TemplateComponent } from './template.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {MatExpansionModule} from '@angular/material/expansion';

import {PositionModule} from '../position/position.module';
import { QuestionModule } from '../question/question.module';
import { BandModule } from '../band/band.module';
import { TemplateSelectComponent } from './template-select/template-select.component';
import { GenericsModule } from '../generics/generics.module';

@NgModule({
  declarations: [TemplateComponent, CreateComponent, EditComponent, ListComponent, TemplateSelectComponent],
  imports: [
    TemplateRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    BrowserModule,
    CommonModule,
    MatSelectModule,
    MatTableModule,
    MatCheckboxModule,
    NgMultiSelectDropDownModule.forRoot(),
      MatExpansionModule,
      PositionModule,
      QuestionModule,
      BandModule,
      GenericsModule
  ],
    exports: [TemplateComponent, TemplateSelectComponent]
})
export class TemplateModule { }
