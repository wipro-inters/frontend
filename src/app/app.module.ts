import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideNavComponent } from './side-nav/side-nav.component';
import { SnackbarCreateComponent } from './snackbar-create/snackbar-create.component';
import { SubareaModule } from './subarea/subarea.module';
import { CoreModule } from './core/core.module';
import { CandidateModule } from './candidate/candidate.module';
import { AreaModule } from './area/area.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PositionModule } from './position/position.module';
import { TemplateModule } from './template/template.module';
import { QuestionModule } from './question/question.module';
import { UserModule } from './user/user.module';
import { CreateComponent } from './role/create/create.component';
import { RoleModule } from './role/role.module';
import { InterviewModule } from './interview/interview.module';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent,
    SnackbarCreateComponent,
    SideNavComponent,
    SnackbarCreateComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    SubareaModule,
    CandidateModule,
    AreaModule,
    QuestionModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    PositionModule,
    TemplateModule,
    QuestionModule,
    InterviewModule,
    UserModule,
    NgMultiSelectDropDownModule.forRoot(),
    RoleModule,
    DashboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
