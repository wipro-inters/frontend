import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ClientService } from './client.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class BandDataService {

    private subject = new BehaviorSubject([]);
    
    bands = this.subject.asObservable();

    constructor(private client: ClientService) {
        this.updateBand();
    }

    updateBand(): void{
        this.client.list().subscribe(bands => this.setBand(bands))
    }

    getBand(id: number){
        return this.bands
            .pipe(map(bands => bands.find(band => band.id === id)));
    } 

    setBand(bands){
        this.subject.next(bands);
    }

}
