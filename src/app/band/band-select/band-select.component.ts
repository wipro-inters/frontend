import { EventEmitter, Component, OnInit, Output} from '@angular/core';
import { ClientService } from '../client.service';

@Component({
    selector: 'app-band-select',
    templateUrl: './band-select.component.html',
})

export class BandSelectComponent implements OnInit {
    bands = [];
    selected = {};

    //Emit selected band on selection change
    @Output() bandChange = new EventEmitter();

    private emptyBand = {id: '', band: ''};

    constructor(private client: ClientService) { }

    ngOnInit(): void {
        this.client.list().subscribe(response => this.bands = [this.emptyBand, ...response]);
    }

    onSelectionChange(event){
        this.bandChange.emit(this.selected);
    }
}
