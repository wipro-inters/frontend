import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BandDataService } from '../band-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
})

export class ListComponent implements OnInit {

  bands;
  columns = ['id', 'band'];
  constructor(private client: ClientService,
              private data: BandDataService,
              private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.client.list().subscribe(response => this.bands = response)
  }

}
