import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { band_list } from '../endpoints';

@Injectable({
    providedIn: 'root',
})
export class ClientService {

    constructor(private http: HttpClient) { }

    list(): Observable<any> {
        return this.http.get<any>(band_list);
    }

}
