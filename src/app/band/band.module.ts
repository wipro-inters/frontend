import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BandRoutingModule } from './band-routing.module';
import { BandComponent } from './band.component';
import { ListComponent } from './list/list.component';
import { BandSelectComponent } from './band-select/band-select.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [BandComponent, ListComponent, BandSelectComponent],
  imports: [
    CommonModule,
    BandRoutingModule,
    CoreModule,
  ],
  exports: [
    BandComponent, BandSelectComponent
  ]
})
export class BandModule { }
